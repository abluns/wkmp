Windows, Kangle, Mysql And PHP (Wkmp)
======================================
wkmp是完全开源，自由，干净的服务器集成环境
------------------------------------------------------

### 当前wkmp版本:

  * Wkmp 1.0.1

----

### 软件版本:

  * Kangle 3.5.21

  * Mysql 5.6.50

  * PHP 7.4.13 (Non Thread Safe + FastCGI)

  * phpMyAdmin 4.8.3
  
  * Redis 5.0.10
  
  * Memcached 1.6.9
  
  * Sendmail 0.32
  
----

### 环境需求：
  * Windows 7/10/Server 2008/2012 x64 或更高
  * 需要右键管理员身份运行
  * [Visual Studio C++ 2017 (x64) Redistributable](https://go.microsoft.com/fwlink/?LinkId=746572)
  * [.NET Framework 4.6](https://www.microsoft.com/en-us/download/details.aspx?id=48130)

----

### 小贴士：

  * Kangle默认用户名和密码：root/123456，端口3311
  * Mysql默认用户名和密码: root/123456
  * You should change $cfg['blowfish_secret'] in config.inc.php in the phpmyadmin folder for added security.
  * Wkmp现在使用VigorTLS提供的SSL证书

[Website](https://gitlab.com/abluns/wkmp)
