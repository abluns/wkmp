<?php
/*
 * Generated configuration file
 */

/* Servers configuration */
$i = 0;

/* Server: MySQL [1] */
$i++;
$cfg['Servers'][$i]['host'] = '127.0.0.1';
$cfg['Servers'][$i]['port'] = '3306';
$cfg['Servers'][$i]['socket'] = '';
$cfg['Servers'][$i]['connect_type'] = 'tcp';
$cfg['Servers'][$i]['auth_type'] = 'cookie';
$cfg['Servers'][$i]['user'] = 'root';
$cfg['Servers'][$i]['password'] = '';
$cfg['Servers'][$i]['compress'] = true;
$cfg['Servers'][$i]['hide_db'] = '(information_schema)';
$cfg['Servers'][$i]['AllowNoPassword'] = false;

/* End of servers configuration */

$cfg['blowfish_secret'] = 'U2FsdGVkX18c4QI5QbEkf+WpKm3qE6UpjBZ42btTXt0=';
$cfg['UploadDir'] = '';
$cfg['SaveDir'] = '';
$cfg['MaxDbList'] = 3000;
$cfg['MaxTableList'] = 300;
$cfg['VersionCheck'] = false;
$cfg['SendErrorReports'] = 'never';
$cfg['DefaultLang'] = 'zh_CN';
$cfg['ServerDefault'] = 1;
?>
